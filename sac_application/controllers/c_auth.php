<?

	/**
	*
	*
	* Central de atendimento ao cliente - Unimed Angra dos Reis
	* @author Marcelo Leonízio - eu@marceloleonizio.com
	*
	* Controller de Autenticação
	*
	**/

	class c_auth extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();

			//Carrega Header da página html
			$this->load->view('login/headLogin');

			// Carrega o Model de verificação de Login
			$this->load->model('login/model_login');

			//Carrega library de validação
			$this->load->library('form_validation');
			

		// Se o usuário estiver logado, redireciona para /contatos
		if($this->session->userdata('user_logado') )
		{
			redirect('/c_unicentral', 'refresh');			
		}

		}

		public function index()
		{	

			//Carrega regras de validação do Model
			$this->model_login->valida_login();
			
			$this->form_validation->set_error_delimiters('<div class="text-error">','</div>');
		
			if( $this->form_validation->run()==FALSE )
		{
				# Não passou nas validações. Abre o formulário de login

				$this->load->view('login/login');
				$this->load->view('login/footerLogin');
		}
		else
		{
			# Passou nas validações e o usuário foi logado com sucesso.

			redirect('/c_unicentral');
		}
		}

		public function r_valida_login($pass)
	{
				
		// Obtêm apenas o o nome de usuario pois a senha já foi recuperada através do parâmetro $senha
		
		$user = $this->input->post('user', TRUE);
		
		// A partir do método verifica_usu() do Model, valida se esse usuário realmente existe
		
		$resultado = $this->model_login->auth_usu($user, $pass);
		
		// $resultado retornar o objeto de get() com alguns valores e um deles é o num_rows
		
		// Grava Cookie de nome do usuario no computador
		$remember = $this->input->post('remember');
		if ($remember == 1) {
		$cookie = array(
                  'name'   => 'SAC/Remember-me',
                  'domain'  =>'desenv.angra.unimed.com.br',
                  'value'  => base64_encode($user),
                  'expire' => '9996598',
                  'path'   => '/',
    			  'prefix' => '',
                  'secure' => FALSE
               );
			
		 	set_cookie($cookie); 
		 }
	  
		if( $resultado==FALSE )
		{
			$this->form_validation->set_message('r_valida_login', 'Não encontramos seu cadastro no sistema.');
			return FALSE;	
		}
		else
		{

			// O usuário existe. Coloca na sessão o seu ID e uma variável 'user_logado' 

				// Grava o Usuario no cookie para lembrar na proxima vez que ele entrar na pagina
			
			$dados_login = array(
							'user_logado' => TRUE, 
							'user_id' => $resultado,
							'user_login' => $user,
							'cdfun' => $CDFUN,

							);

			$this->session->set_userdata($dados_login);	
	
			

			// Retorna TRUE para que a validação seja realizada com sucesso.

			
			return TRUE;
		}
	}




	}