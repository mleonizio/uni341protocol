<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');


    /**
     * Sistema de Protocolo de Atendimento Unimed Angra dos Reis.
     * displays por Marcelo Leonizio<strong>mleonizio@gmail.com</strong>
     * @author Marcelo Leonizio <mleonizio@angra.unimed.com.br>
     * @copyright Copyright (c) 2013, Marcelo Leonízio
     *
     * classe c_unicentral controller principal da aplicação
    */
    

    class c_unicentral extends CI_Controller
    {
        
        function __construct()
        {
            parent::__construct();

            $this->load->model('model_unicentral');
            $this->load->model('model_uniupdt');

            $this->load->view('headMain');
        }

        public function index()
        {
            $this->load->view('navMain');
            $this->load->view('index');
            $this->load->view('footerMain');
        }


        public function c_getNomeCliente()
        {
            $this->load->view('navMain');
            $this->load->view('headResultBusca');

            if($_GET){
                extract($_GET);
                $qnome = strtoupper($queryNome);

                $retornaNomeCliente = $this->model_unicentral->getNomeCliente($qnome);

                if ($retornaNomeCliente->num_rows() > 0)
                {
                    foreach ($retornaNomeCliente->result() as $rNomeCliente)
                    {
                        $dataCliente = array
                        (
                            'nomeCliente'   =>  $rNomeCliente->NOPESSOA,
                            'areaacao'      =>  $rNomeCliente->CDAREAACAO,
                            'nrcontrato'    =>  $rNomeCliente->NRCONTRATO,
                            'nrfamilia'     =>  $rNomeCliente->NRFAMILIA,
                            'tpusu'         =>  $rNomeCliente->TPUSUARIO,
                            'notpusuario'   =>  $rNomeCliente->NOTPUSUARIO,
                            'meiocontato'   =>  $rNomeCliente->MEIOCONTATO,
                            'cdcgc_cpf'     =>  $rNomeCliente->NRCPF
                            );  

                        $this->load->view('resultBusca', $dataCliente);
                                        // $this->recUsuUni($dataCliente);
                    };
                    $this->load->view('footerMain');
                }else{
                    $this->session->set_flashdata('no-existe', 'Não há cadastro desse cliente, favor entrar em contato com o setor responsável.');
                    redirect('c_unicentral/geraAtendimento');

                }

            }
        }

        public function c_getCdCliente()
        {

            if($_GET){

                extract($_GET);

                $usuCart    = explode(".", $cd_cliente);
                $areaacao   = $usuCart[0];
                $empresa    = $usuCart[1];
                $familia    = $usuCart[2];
                $tpusu      = $usuCart[3];

                if ($areaacao <> "341") {
                    
                    $dataCliente = array
                    (
                        'nomeCliente'   =>  "CLIENTE INTERCÂMBIO",
                        'areaacao'      =>  $areaacao,
                        'nrcontrato'    =>  $empresa,
                        'nrfamilia'     =>  $familia,
                        'tpusu'         =>  $tpusu,
                        'cgc_cpf'       =>  "DIGITE CPF",
                        'meiocontato'   =>  "CONTATO CLIENTE",
                        );

                    $this->recUsuUni($dataCliente);
                }else{          

                    $getCdCli = $this->model_unicentral->getUsuCarteirinha($areaacao, $empresa, $familia, $tpusu);

                    if ($getCdCli->num_rows() > 0)
                    {
                        foreach ($getCdCli->result() as $rCdCliente)
                        {
                            $dataCliente = array
                            (
                                'nomeCliente'   =>  $rCdCliente->NOPESSOA,
                                'areaacao'      =>  $rCdCliente->CDAREAACAO,
                                'nrcontrato'    =>  $rCdCliente->NRCONTRATO,
                                'nrfamilia'     =>  $rCdCliente->NRFAMILIA,
                                'tpusu'         =>  $rCdCliente->TPUSUARIO,
                                'cgc_cpf'       =>  $rCdCliente->NRCPF,
                                'meiocontato'   =>  $rCdCliente->MEIOCONTATO,
                                );

                                        // print_r($dataCliente)    ;

                            $this->recUsuUni($dataCliente);
                        };
                    }else{

                        $this->session->set_flashdata('no-existe', 'Não há cadastro desse cliente, favor entrar em contato com o setor responsável.');
                        redirect('c_unicentral/geraAtendimento/');
                    }   

                }
            }
        }

        function recUsuUni($dataCliente)
        {

            $this->load->view('navMain');

            $dataCliente['setor'] = $this->db->get('UNI341_SETOR');
            $dataCliente['motivo'] = $this->db->get('UNI341_SAC_MOTIVO');
                // $m_setor['categoria'] = $this->db->get('UNI341_SAC_CATEGORIA');
            $this->load->view('atendimento/geraAtendimento', $dataCliente);
            $this->load->view('footerMain');
        }

        public function geraAtendimento()
        {
            $this->load->view('navMain');

            $dataCliente['setor'] = $this->db->get('UNI341_SETOR');
            $dataCliente['motivo'] = $this->db->get('UNI341_SAC_MOTIVO');
    
        // $m_setor['categoria'] = $this->db->get('UNI341_SAC_CATEGORIA');
            $this->load->view('atendimento/geraAtendimento', $dataCliente);
            $this->load->view('footerMain');
        }

        public function gravaChamado()
        {
            extract($_POST);                    
            redirect('c_unicentral/confereDadosUsuOcorrencia/'.$cdcgc_cpf);
        }

        public function confereDadosUsuOcorrencia()
        {

            extract($_POST);

            $dadosUsuOcorrencia = $this->model_unicentral->confereDadosOcorrencia($cdcgc_cpf);

            if ($dadosUsuOcorrencia->num_rows() > 0)
            {

                if ($dadosUsuOcorrencia->row(0)->NRCGC_CPF == $cdcgc_cpf) 
                {
                    $this->session->set_flashdata('existe-ocorrencia', 'O Sr(a) '.$dadosUsuOcorrencia->row(0)->NOME_CLIENTE.' ja possui uma ou mais ocorrencias em seu histórico de cliente, aguarde você será redirecionado....');
                    redirect('c_unicentral/redirectPageID/'.$cdcgc_cpf);

                }       

            }else{

                $cdCliente = explode(".", $cd_cliente);

                $cdareaacao = $cdCliente[0];
                $nrcontrato = $cdCliente[1];
                $nrfamilia  = $cdCliente[2];
                $tpusuario  = $cdCliente[3];

                $dataAberturaOcorrencia = "SYSDATE";

                $null = "";

                $dadosUsuWeb = $this->model_unicentral->recDadosUsuWeb();

                $protocolo_ano_atual = "TO_CHAR(SYSDATE, 'YYYY')";  
                $protocolo_mes_atual = "TO_CHAR(SYSDATE, 'MM')";    

                $seq_protocolo =  $protocolo_ano_atual."||".$protocolo_mes_atual."||Lpad(SQ_UNI341_PROTOCOLO_SEQ.NEXTVAL,6, '0000000')";

                $this->db->set('NR_PROTOCOLO', $seq_protocolo, FALSE);
                $this->db->set('DATA_ABERTURA', $dataAberturaOcorrencia, FALSE);
                $this->db->set('CDAREAACAO', $cdareaacao);
                $this->db->set('NRCONTRATO', $nrcontrato);
                $this->db->set('NRFAMILIA', str_pad($nrfamilia,6,'0', STR_PAD_LEFT));
                $this->db->set('TPUSU', $tpusuario);
                $this->db->set('NOME_CLIENTE', $nopessoa);
                $this->db->set('CONTATO_CLIENTE', $meiocontato);
                $this->db->set('CD_MOTIVO_SAC', $diaLimiteMotivo);
                $this->db->set('HISTORICO_SAC', $historico_sac);
                $this->db->set('ID_USU_ORIGEM', $dadosUsuWeb->row(0)->CDFUNCIONARIO);
                $this->db->set('ID_SETOR_ORIGEM',$dadosUsuWeb->row(0)->CD_SETOR);
                $this->db->set('ID_SETOR_ATEND', $dadosUsuWeb->row(0)->CD_SETOR);
                $this->db->set('DATA_LIMITE', $data_limite);
                $this->db->set('DATA_FECHAMENTO', $null);
                $this->db->set('HISTORICO_CONCLUSAO', $null);
                $this->db->set('NR_PROT_ORIGEM', $null);
                $this->db->set('NRCGC_CPF', $cdcgc_cpf);
                $this->db->set('NR_PROT_GRAVACAO', $null);
                $this->db->set('CD_STATUS', '0');
                $this->db->set('CD_PRIORIDADE', $null);

                $this->model_unicentral->insereDadosOcorrencia();
                redirect('c_unicentral/viewOcorrencia/'.$cdcgc_cpf);
            }
        }

        public function detalheUsuario($cdcgc_cpf)
        {

            $this->load->view('navMain');
            $viewUsu = $this->model_unicentral->getIdViewDetUsu($cdcgc_cpf);
            $count = $this->model_unicentral->contaOcorrencias($cdcgc_cpf);
            $totalCount = $count->num_rows();
            @$totalResult = $count->row(0)->CD_STATUS;
            switch ($totalResult) {
                case '1':
                $UsuData['aberto']   ='0'.$count->num_rows();
                $UsuData['pendente'] = '00';
                $UsuData['fechado']  = '00';
                break;
                case '2':
                $UsuData['aberto']   = '00';
                $UsuData['pendente'] ='0'.$count->num_rows();
                $UsuData['fechado']  = '00';
                break;
                case '3':
                $UsuData['aberto']   = '00';
                $UsuData['pendente'] = '00';
                $UsuData['fechado']  ='0'.$count->num_rows();
                break;                  

                default:
                $UsuData['aberto']   = '00';
                $UsuData['pendente'] = '00';
                $UsuData['fechado']  = '00';
                break;
            }

            $UsuData['cdcgc_cpf']           = $viewUsu->row(0)->NRCPF;
            $UsuData['nocontrato']          = $viewUsu->row(0)->NOCONTRATO;
            $UsuData['nrcontrato']          = $viewUsu->row(0)->NRCONTRATO;
            $UsuData['nopessoa']            = $viewUsu->row(0)->NOPESSOA;
            $UsuData['dtinicio']            = $viewUsu->row(0)->DTINICIO;
            $UsuData['noabreviado']         = $viewUsu->row(0)->NOABREVIADO;
            $UsuData['nrfamilia']           = $viewUsu->row(0)->NRFAMILIA;
            $UsuData['notpusuario']         = $viewUsu->row(0)->NOTPUSUARIO;
            $UsuData['nomemae']             = $viewUsu->row(0)->NOMEMAE;
            $UsuData['dtnascimento']        = $viewUsu->row(0)->DTNASCIMENTO;
            $UsuData['idade']               = $viewUsu->row(0)->IDADE;
            $UsuData['nocidade']            = $viewUsu->row(0)->NOCIDADE;
            $UsuData['nobairro']            = $viewUsu->row(0)->NOBAIRRO;
            $UsuData['nrcep']               = $viewUsu->row(0)->NRCEP;
            $UsuData['nrcpf']               = $viewUsu->row(0)->NRCPF;
            $UsuData['nologradouro']        = $viewUsu->row(0)->NOLOGRADOURO;
            $UsuData['areacao_unimed']      = $viewUsu->row(0)->AREACAO_UNIMED;
            $UsuData['noacomodacao']        = $viewUsu->row(0)->NOACOMODACAO;
            $UsuData['cdestado']            = $viewUsu->row(0)->CDESTADO;
            $UsuData['noestadocivil']       = $viewUsu->row(0)->NOESTADOCIVIL;
            $UsuData['notpcoparticipacao']  = $viewUsu->row(0)->NOTPCOPARTICIPACAO;
            $UsuData['noabrangencia']       = $viewUsu->row(0)->NOABRANGENCIA;
            $UsuData['meiocontato']         = $viewUsu->row(0)->MEIOCONTATO;


            $this->load->view('viewDetUsu', $UsuData);
            $this->load->view('footerMain');
        }

        public function redirectPageID($cdcgc_cpf)
        {
            $viewCli = $this->model_unicentral->getIdViewDetCli($cdcgc_cpf);
            $cliData['cdcgc_cpf'] = $viewCli->row(0)->NRCGC_CPF;
            $this->load->view('viewRedirectPage',$cliData);
        }

        public function viewOcorrencia($cdcgc_cpf)
        {
            $this->load->view('navMain');
            $abreOcor = $this->model_unicentral->getIdViewDetCli($cdcgc_cpf);
            $ocorrenciaData['seq_protocolo'] = $abreOcor->row(0)->NR_PROTOCOLO;
            $this->load->view('editarOcorrencia', $ocorrenciaData);

            $this->load->view('footerMain');
        }

        public function historicoCliente($cdcgc_cpf)
        {
            $this->load->view('navMain');

            $viewCli = $this->model_unicentral->getIdViewDetCli($cdcgc_cpf);
            $cliData['cdcgc_cpf'] = $viewCli->row(0)->NRCGC_CPF;
            $cliData['nomeCliente'] = $viewCli->row(0)->NOME_CLIENTE;
            $this->load->view('historicoCliente',$cliData);

            $this->load->view('footerMain');
        }

        public function resultadoBuscaChamado()
        {
            $this->load->view('navMain');
            $this->load->view('chamado/headResultBuscaChamado');

            extract($_GET);

                //  echo $tipo;
            $vw_status = $this->model_unicentral->buscaChamado($tipo);

            foreach ($vw_status->result() as $field_status) {

                if ($field_status->CD_STATUS == 0) {
                    $field_status->CD_STATUS = "SEM ATENDENTE";
                }elseif ($field_status->CD_STATUS == 1) {
                    $field_status->CD_STATUS = "ABERTO";
                }elseif ($field_status->CD_STATUS == 2) {
                    $field_status->CD_STATUS = "FECHADO";
                }elseif ($field_status->CD_STATUS == 3) {
                    $field_status->CD_STATUS = "CANCELADO";
                }

                $loadStatus['nrprotocolo'] = $field_status->NR_PROTOCOLO;
                $loadStatus['setor']    = $field_status->ID_SETOR_ORIGEM;
                $loadStatus['resumo']   = $field_status->HISTORICO_SAC;
                $loadStatus['dataAbertura'] = $field_status->DATA_ABERTURA;
                $loadStatus['status']   = $field_status->CD_STATUS;
                $loadStatus['motivo']   = $field_status->NO_MOTIVO_SAC;
                $loadStatus['cdcgc_cpf'] = $field_status->NRCGC_CPF;

                $this->load->view('chamado/resultBuscaChamado', $loadStatus);

            }

            $this->load->view('footerMain');
        }

        public function manutencaoChamado($nrprotocolo, $cdcgc_cpf)
        {
            $dadosManutChamado['dados'] = $this->model_unicentral->manutencaoChamado($nrprotocolo, $cdcgc_cpf);

            $this->load->view('navMain');
            $this->load->view('chamado/consultaChamado', $dadosManutChamado);
            $this->load->view('chamado/footerChamado'); 
        }

        public function logout()
        {
            $this->session->sess_destroy();
            redirect('c_auth/', 'refresh');
        }

        public function charts()
        {

            $this->load->view('charts');
            $this->load->view('footerMain');
        }

        public function tipoConta()
        {

            $this->load->view('login/teste.php');
        }

    }

    /* End of file c_unicentral.php */
/* Location: ./application/controllers/c_unicentral.php */