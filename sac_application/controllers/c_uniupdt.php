<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');


	/**
	 * Sistema de Protocolo de Atendimento Unimed Angra dos Reis.
	 * displays por Marcelo Leonizio<strong>mleonizio@gmail.com</strong>
	 * @author Marcelo Leonizio <mleonizio@angra.unimed.com.br>
	 * @copyright Copyright (c) 2013, Marcelo Leonízio
	 *
	 * classe c_unicentral controller principal da aplicação
 	*/
	
	
	class c_uniupdt extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('model_uniupdt');
			}


		public function encerraChamado($nrprotocolo)
		{
			extract($_POST);

			$this->model_uniupdt->encerraChamado($nrprotocolo, $txtComplemento);

			$this->session->set_flashdata('encerra-chamado', 'Chamado '.$nrprotocolo.' encerrado com sucesso!');
			redirect('c_unicentral/');
			
		}

		public function cancelaChamado($nrprotocolo)
		{
			
			extract($_POST);

			$this->model_uniupdt->cancelaChamado($nrprotocolo, $txtComplemento);

			$this->session->set_flashdata('cancela-chamado', 'Chamado '.$nrprotocolo.' cancelado com sucesso!');
			redirect('c_unicentral/');
			
		}

		public function insereComplemento($nrprotocolo,$cgc_cpf)
		{
			extract($_POST);

			$dt_movimento = "SYSDATE";
			$null = "";
			$txt = "<p>".$txtComplemento."</p>";

			$this->db->set('NR_PROTOCOLO', $nrprotocolo);
			$this->db->set('DATA_MOVIMENTO', $dt_movimento, FALSE);
			$this->db->set('HISTORICO_SAC', $txt);
			$this->db->set('ID_USU_MOVIMENTO',strtoupper($this->session->userdata('user_login')));
			$this->db->set('ID_USU_ORIGEM', $usuOrigem);
			$this->db->set('ID_USU_ATEND_OLD', $atendOld);
			$this->db->set('ID_USU_ATEND_NEW', $null);
			$this->db->set('ID_SETOR_OLD', $null);
			$this->db->set('ID_SETOR_NEW', $setorAtual);
			$this->db->set('ID_MOVIMENTO', $tipoMovimento);

			$insere_d = $this->model_uniupdt->insereComplemento();
			$this->session->set_flashdata('insere-complemento', 'Dados complementares adicionados com sucesso ao Chamado');
			redirect('c_unicentral/manutencaoChamado/'.$nrprotocolo."/".$cgc_cpf);
		}


	}
