<?
	/**
	* 
	*/

	class model_unicentral extends CI_Model
	{
		
		private $t_vw_usuarios = "VW_UNI341_DADOS_USUARIOS";
		private $t_sac 		   = "UNI341_SAC";
		private $t_motivo 	   = "UNI341_SAC_MOTIVO";
		private $t_usuweb 	   = "F_UNI341_USUWEB";
		private $t_setor	   = "UNI341_SETOR";


		public function getNomeCliente($queryNome)
		{
			$this->db->like('NOPESSOA', $queryNome, 'both');
			$this->db->where('DTEXCLUSAO is Null');
			// $this->db->limit(100);
			return $this->db->get($this->t_vw_usuarios);	
		}

		public function getUsuCarteirinha($areaacao, $empresa, $familia, $tpusu)
		{
			$this->db->where("v.CDAREAACAO".'||'."v.NRCONTRATO".'||'."Lpad(v.NRFAMILIA,6,'0')". '||'."v.TPUSUARIO", "'$areaacao'"."||"."'$empresa'"."||"."'$familia'"."||"."'$tpusu'", FALSE);
			$this->db->where('DTEXCLUSAO is Null');

			return $this->db->get($this->t_vw_usuarios.' v');

			// if ($in == TRUE) {

			// 		redirect('c_unicentral/c_getCdCliente');
			
			// }else{
			// 	$this->db->_error_message($in); 
			// }

		}

		function getIdViewDetUsu($cdcgc_cpf)
		{
		
			$this->db->where('NRCPF', $cdcgc_cpf);
			$this->db->where('DTEXCLUSAO is Null');
				$resultado = $this->db->get($this->t_vw_usuarios);
			
				if( $resultado->num_rows==0 )
					{
						return FALSE;
					}
				else
					{
						return $resultado;	
					}
		}

		function getIdViewDetCli($cdcgc_cpf)
		{
		
			$this->db->where('NRCGC_CPF', $cdcgc_cpf);
				$resultado = $this->db->get($this->t_sac);
			
				if( $resultado->num_rows==0 )
					{
						return FALSE;
					}
				else
					{
						return $resultado;	
					}
		}

		public function viewDetalheUsuarios($cdcgc_cpf)
		{
			$this->db->select('NRFAMILIA, NOLOGRADOURO, NRCEP, CDESTADO, NOMEMAE, TPUSUARIO, DTNASCIMENTO, IDADE, NOTPCOPARTICIPACAO, NOACOMODACAO, NOCONTRATO, NOBAIRRO, NOCIDADE, AREACAO_UNIMED , MEIOCONTATO');
			$this->db->where('NRCPF',$cdcgc_cpf);
			return $this->db->get($this->t_vw_usuarios);
		}

		public function contaOcorrencias($cdcgc_cpf)
		{
			$this->db->select('CD_STATUS', 1);
			$this->db->where('NRCGC_CPF', $cdcgc_cpf);
			return $this->db->get($this->t_sac);
		}

		public function confereDadosOcorrencia($cdcgc_cpf)
		{
			$this->db->where('NRCGC_CPF', $cdcgc_cpf);
			return $this->db->get($this->t_sac);
		}

		public function insereDadosOcorrencia()
		{	

			$this->db->insert($this->t_sac);
						
			// if ($in == TRUE) {

			// $this->session->set_flashdata('abreOcorrencia-ok','Abertura de Ocorrencia realizada, anote o número do protocolo');
			// redirect('c_unicentral/recDadosUsuUni');
			// }else{
			// 	$this->db->_error_message($in); 
			// }
		}

		public function recDadosUsuWeb()
		{
			$this->db->where('NOFUNCIONARIO', $this->session->userdata('user_id'));
			return $this->db->get($this->t_usuweb);
		}


		public function getSetor()
		{
			$this->db->get($this->t_setor);
		}


		public function buscaChamado($tipo)
		{
			
			if ($tipo == 4) {

			return $this->db->query("SELECT s.*,
 				(SELECT m.no_motivo_sac FROM uni341_sac_motivo m
  					WHERE m.cd_motivo_sac = s.cd_motivo_sac) \"NO_MOTIVO_SAC\"
					FROM uni341_sac s");
			
			}else{

			return $this->db->query("SELECT s.*,
 				(SELECT m.no_motivo_sac FROM uni341_sac_motivo m
  					WHERE m.cd_motivo_sac = s.cd_motivo_sac) \"NO_MOTIVO_SAC\"
					FROM uni341_sac s where s.cd_status = '$tipo'");
			}
		}

		public function manutencaoChamado($nrprotocolo, $cdcgc_cpf)
		{
			return $this->db->query("SELECT d.*,u.nofuncionario \"NO_USU_MOVIMENTO\", u2.nofuncionario \"NO_USU_ORIGEM\", u3.nofuncionario \"NO_USU_ATEND_OLD\", u4.cdfuncionario \"CD_USU_ATEND_OLD\", s.no_setor \"NO_SETOR_ATUAL\", s2.no_setor \"NO_SETOR_ORIGEM\", c.nome_cliente \"NOME_CLIENTE\",
									c.historico_sac \"MOT_ABERTURA_INICIAL\", c.data_abertura \"DATA_ABERTURA\", c.nrcgc_cpf \"CPF\", c.contato_cliente \"CONTATO\",
       									(SELECT t.txdescricao FROM tabela_de_codigo t
        					  		WHERE t.cdidentificador='341IDMOVIMENTOSACDETALHE' and t.cdvalor=d.id_movimento) \"DESC_MOVIMENTO\"
									FROM uni341_sac_detalhe d, f_uni341_usuweb u, f_uni341_usuweb u2  , f_uni341_usuweb u3, f_uni341_usuweb u4, uni341_sac c, uni341_setor s, uni341_setor s2
     								WHERE exists(SELECT 1 FROM uni341_sac s
                  					WHERE s.nr_protocolo='$nrprotocolo' and d.nr_protocolo='$nrprotocolo' and s.nrcgc_cpf = '$cdcgc_cpf' and c.nrcgc_cpf = '$cdcgc_cpf')
    				 				and d.id_usu_movimento = u.cdfuncionario (+)
    				 				and d.id_usu_origem = u2.cdfuncionario (+)
    				 				and d.id_usu_atend_old = u3.cdfuncionario (+)
    				 				and d.id_usu_atend_old = u4.cdfuncionario (+)
    				 				and c.id_setor_atend = s.cd_setor (+)
    				 				and c.id_setor_origem = s2.cd_setor (+)	ORDER BY d.DATA_MOVIMENTO desc ");

		}

	}