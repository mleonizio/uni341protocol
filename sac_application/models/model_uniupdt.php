<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	/**
	 * Sistema de Protocolo de Atendimento Unimed Angra dos Reis.
	 * displays por Marcelo Leonizio<strong>mleonizio@gmail.com</strong>
	 * @author Marcelo Leonizio <mleonizio@angra.unimed.com.br>
	 * @copyright Copyright (c) 2013, Marcelo Leonízio
	 *
	 * classe model_uniupdt model especifico para updates e inserts da parte de Chamados do sistema
 	*/
		
		class model_uniupdt extends CI_Model
		{

			
			private $t_sac 		   = "UNI341_SAC";
			private $t_sac_d	   = "UNI341_SAC_DETALHE";
			

			public function encerraChamado($nrprotocolo, $txtComplemento)
			{

				$this->db->query("UPDATE UNI341_SAC s 
								  SET s.CD_STATUS = 2, s.DATA_FECHAMENTO = SYSDATE
								  WHERE s.NR_PROTOCOLO = $nrprotocolo");
				
				$this->db->select('ID_USU_ORIGEM');
				$this->db->where('NR_PROTOCOLO', $nrprotocolo);
				$usuOrigem = $this->db->get($this->t_sac);

				$txt = "<p>".$txtComplemento."</p>";

				$this->db->set('NR_PROTOCOLO', $nrprotocolo);
				$this->db->set('DATA_MOVIMENTO', "SYSDATE", FALSE);
				$this->db->set('HISTORICO_SAC', $txt);
				$this->db->set('ID_USU_ORIGEM', $usuOrigem->row(0)->ID_USU_ORIGEM);
				$this->db->set('ID_USU_MOVIMENTO', strtoupper($this->session->userdata('user_login')));
				$this->db->set('ID_MOVIMENTO', '04');
				
				return $this->db->insert($this->t_sac_d);
				
			}


			public function cancelaChamado($nrprotocolo, $txtComplemento)
			{

				$this->db->query("UPDATE UNI341_SAC s 
								  SET s.CD_STATUS = 3, s.DATA_FECHAMENTO = SYSDATE
								  WHERE s.NR_PROTOCOLO = $nrprotocolo");
				
				$this->db->select('ID_USU_ORIGEM');
				$this->db->where('NR_PROTOCOLO', $nrprotocolo);
				$usuOrigem = $this->db->get($this->t_sac);

				$txt = "<p>".$txtComplemento."</p>";

				$this->db->set('NR_PROTOCOLO', $nrprotocolo);
				$this->db->set('DATA_MOVIMENTO', "SYSDATE", FALSE);
				$this->db->set('HISTORICO_SAC', $txt);
				$this->db->set('ID_USU_ORIGEM', $usuOrigem->row(0)->ID_USU_ORIGEM);
				$this->db->set('ID_USU_MOVIMENTO', strtoupper($this->session->userdata('user_login')));
				$this->db->set('ID_MOVIMENTO', '07');
				
				return $this->db->insert($this->t_sac_d);
				
			}

			public function insereComplemento()
			{	
				return $this->db->insert($this->t_sac_d);
			}
				
		}	
	
	/* End of file model_uniupdt.php */
	/* Location: ./application/models/model_uniupdt.php */
?>