<?
	/**
	*
	*
	* Central de Atendimento ao Cliente - Unimed Angra dos Reis
	* @author Marcelo Leonizio - eu@marceloleonizio.com
	*
	*
	* 
	*
	*/

	
	class model_login extends CI_Model
	{
		

		private $tabela = 'F_UNI341_USUWEB';

		public function valida_login()
		{
			// Regras de Validação do login
			// $this->form_validation->set_error_delimiters('<div class="alert-error alert fade in"><button type="button" class="close" data-dismiss="alert">×</button>','</div>');

			$this->form_validation->set_rules('user','Usuário:','required|trim|xss_clean|');
			$this->form_validation->set_rules('pass','Senha:','required|trim|md5|xss_clean|callback_r_valida_login');
		}

		public function auth_usu($user, $pass)
		{
			//converte para minusculo o nome de usuario
			strtolower($user);
			
			// Condição para liberar acesso
			// if ($user == "mleonizio"   || $user == "romeu"   ||  $user == "boliveira"  || $user == "wagner"   || $user == "davi" || $user == "hcezar" ) {
		
				$where = array( 'CDFUNCIONARIO' => strtoupper($user), 'AOSENHAWEBANGRA' => strtoupper($pass) );
				$this->db->where($where);
			
				$resultado = $this->db->get($this->tabela);


				// Retorna FALSE se o usuário não existe no banco de dados
				if( $resultado->num_rows==0 )
				{
					return FALSE;
				}
				else
				{
					// Se o usuário existe, retorna o seu ID
					
					return $resultado->row(0)->NOFUNCIONARIO;

					$dados_session = array(						
							'cdfun' => $resultado->row(0)->CDFUNCIONARIO,
							);

					return $this->session->set_userdata($dados_session);						
				}
			// }else{

			// 	$this->session->set_flashdata('no-auth','Ops.. Você não tem permissão de acesso ao sistema.');
			// 	redirect('c_auth/');
			// }

		}
	}