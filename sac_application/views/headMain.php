	<? 

	/**
	*
	*
	* Central de atendimento ao cliente - Unimed Angra dos Reis
	* @author Marcelo Leonízio - eu@marceloleonizio.com
	*
	*
	**/

	echo doctype('html5');
	?>
<html lang="pt-br">
<head>
	<?
	$meta = array(
	 array('name' => 'description', 'content' => 'Serviço de atendimento ao consumidor - Unimed Angra dos Reis'),
	 array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv'),
	 array('name' => 'viewport','content' => 'width=device-width, initial-scale=1'),
	 array('name' => 'author','content' => 'Marcelo Leonizio - @mleonizio')
  	);	
	echo meta($meta);
	?>

	<title>Central de Atendimento ao Cliente - Unimed Angra dos Reis</title>
	<link rel="shortcut icon" href="<? echo base_url('assets/img/favicon.ico')?>" />
	<? echo link_tag('assets/css/bootplus.css'); ?>
	<? echo link_tag('assets/css/bootstrap-responsive.css'); ?>
	<? echo link_tag('assets/css/DT_bootstrap.css'); ?>
	<? echo link_tag('assets/css/font-awesome.min.css'); ?>
	<? echo link_tag('assets/css/canvas.css'); ?>
	<? echo link_tag('assets/css/util.css')?> 
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.1/css/font-awesome.css" rel="stylesheet">
	
	 <style type="text/css">
      body {
        padding-top: 60px;  /*60px pra fazer o container desgrudar do topbar */
      }		
	</style>

    	<?

    		if( $this->session->flashdata('no-logado') != "")
			{
				echo "<div class='alert-error alert fade in'>
			            <button type='button' class='close' data-dismiss='alert'>×</button>".$this->session->flashdata('no-logado')."</div>";
			}

    	?>
   <script type="text/javascript" charset="utf-8" language="javascript" src='<? echo(base_url('assets/js/inputMask/jquery1.6.js')); ?>'></script>
    <script type="text/javascript" charset="utf-8" language="javascript" src='<? echo(base_url('assets/js/inputMask/masked.js')); ?>'></script>

</head>
	<body>