<div class="container-fluid">
<div class="page-header"><h1>Dados do Chamado</h1>
<p class="lead muted"><small> Gerencie dados do chamado, inserir e solicitar posição atual do chamado</small></p>
</div>
</div>

<div class="span3">
<p><small class="lead muted"> Atendente Atual: <strong> <? echo strtoupper($this->session->userdata('user_login'))?> </small> </strong></p>
	<div class="well well-small">
	<h5>Cliente: <b> <? echo $dados->row(0)->NOME_CLIENTE ?></h5></b>
	<h5>CPF: <b> <? echo $dados->row(0)->CPF ?></h5></b>
	<h5>Número Protocolo: <b class="text-error"> <? echo $dados->row()->NR_PROTOCOLO ?></h5></b>
	<h5>Data Abertura: <b> <? echo $dados->row(0)->DATA_ABERTURA ?></h5></b>
	<h5>Contato: <b><? echo $dados->row(0)->CONTATO ?></b></h5>
	<hr>
	<p class="lead muted"> Motivo da abertura Inicial
	<? echo $dados->row(0)->MOT_ABERTURA_INICIAL?>
	</p>
</div>
</div>
<div class="container">
<div class="container-fluid span8">
 <!-- ### BTNS COMPLEMENTOS -->
			<a  class="btn btn-danger" href="#manutEncerraChamado" role="button" data-toggle="modal">
			 <i class="icon-ok"></i> Encerrar Chamado</a>
				<a class="btn" href="#manutChamadoComplemento" role="button" data-toggle="modal">Complemento</a>
				<a class="btn" href="#manutChamadoSolicitaPosicao" role="button" data-toggle="modal">Solicita Posição</a>
				<a class="btn btn-warning" href="#manutChamadoCancela" role="button" data-toggle="modal">Cancelar</a>
		 <!-- ### BTNS COMPLEMENTOS -->
	<div class="accordion" id="accordion2">
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
				Histórico do Chamado
			</a>
			
		</div>
		<div id="collapseOne" class="accordion-body collapse in">
			<div class="accordion-inner">
	<?
					if( $this->session->flashdata('insere-complemento') != "")
					 {
						echo "<div class='alert-success alert fade in'>
								<button type='button' class='close' data-dismiss='alert'>×</button>".$this->session->flashdata('insere-complemento')."</div>";
					}
	?>
<!-- ### INNER COLLAPSE -->
<? foreach ($dados->result() as $d) {?>
			 <div class="accordion" id="accordion1">
				<div class="accordion-group">
					<div class="accordion-heading">
						<b class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="">
							<? echo strtoupper($d->DESC_MOVIMENTO)?> - <strong> <? echo $d->DATA_MOVIMENTO ?> </strong>
						</b>
					</div>
					<div  class="accordion-body collapse in">
						<div class="accordion-inner">
						 <table class="table table-condensed">
							<tr>
							<td> <strong>Atendente Anterior: </strong> <? echo $d->NO_USU_ATEND_OLD ?></td>
							<td> <strong>Atendente Origem: </strong> <? echo $d->NO_USU_ORIGEM ?></td>
							</tr>
								<tr>
								<td><strong>Setor Atual:</strong> <? echo $d->NO_SETOR_ATUAL ?></td>
								<td><strong>Setor Origem:</strong> <? echo $d->NO_SETOR_ORIGEM ?></td>
								</tr>
						 </table>
						 <table class"table table-condensed" >
							<tr>
								<td><strong>Observações:</strong> <? echo $d->HISTORICO_SAC?> </td>
							</tr>
						 </table>
						</div>
					</div>
				</div>
			</div>
<? }?>

			</div>
		</div>
	</div>
		</div>    
	</div>

		<!-- INICIO / Modal -  Encerrar Chamado -->
	<form method="POST" action="<? echo(site_url('c_uniupdt/encerraChamado/'.$dados->row(0)->NR_PROTOCOLO."/".$dados->row(0)->CPF)); ?>" >
		<div id="manutEncerraChamado" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h1 class="muted">Encerrar Chamado</h1>
		<table class="table-condensed">
				<tbody>
					<tr>
						<td> <strong> Usuário Atual: </strong> <br> <? echo strtoupper($this->session->userdata('user_login')); ?> </td>
						<td> <strong> Usuário Origem: </strong> <br> <? echo strtoupper($dados->row(0)->ID_USU_ORIGEM) ?> </td>
						<td> <strong> Data: </strong> <br> <? echo date("d/m/Y H:i:s"); ?></td>
					</tr>
				</tbody>
			</table>
		<hr>
		<div class="modal-body">
				<p> Informe qual o Motivo do Fechamento do Chamado: </p>
				<div class="input-append">
					<textarea  name="txtComplemento" id="txtComplemento" style="width:520px;" rows="10"> </textarea><br>
					<input type="hidden" name="tipoMovimento" value="04">
					<input type="hidden" name="usuOrigem" value="<? echo $dados->row(0)->ID_USU_ORIGEM ?>">
				</div>
		</div> 
			<div class="modal-footer">
				<input type="submit" class="btn btn-danger" value="Encerrar Chamado">
				<!--<small class="muted">Teste bla bla bla blab lab  balb</small>-->
			</div>
		</div>
	</form>
<!-- FIM / Modal -  Cancela Chamado -->

	<!-- INICIO / Modal -  Complemento Chamado -->
		<div id="manutChamadoComplemento" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	 <h1 class="muted">Inserir Complemento</h1>
		<p class="muted">Inserir complemento no Chamado .</p>
		<table class="table-condensed">
				<tbody>
					<tr>
						<td> <strong> Usuário Atual: </strong> <br> <? echo strtoupper($this->session->userdata('user_login')); ?> </td>
						<td> <strong> Usuário Origem: </strong> <br> <? echo strtoupper($dados->row(0)->ID_USU_ORIGEM) ?> </td>
						<td> <strong> Data: </strong> <br> <? echo date("d/m/Y H:i:s"); ?></td>
					</tr>
				</tbody>
			</table>
		<hr>
		<div class="modal-body">
			<form method="POST" action="<? echo(site_url('c_uniupdt/insereComplemento/'.$dados->row(0)->NR_PROTOCOLO."/".$dados->row()->CPF)); ?>" class="form-search">
				<div class="input-append">
					<textarea  name="txtComplemento" id="txtComplemento" style="width:520px;" rows="10"> </textarea><br>
					<input type="hidden" name="tipoMovimento" value="02">
					<input type="hidden" name="usuOrigem" value="<? echo $dados->row(0)->ID_USU_ORIGEM ?>">
					<input type="hidden" name="setorAtual" value="<? echo $this->session->userdata('setorAtual') ?>">
					<input type="hidden" name="atendOld" value="<? echo $dados->row(0)->CD_USU_ATEND_OLD ?>">
				</div>
		</div> 
	<div class="modal-footer">
	<button type="submit" class="btn btn-success">Inserir Complemento</button>
	<!--<small class="muted">Teste bla bla bla blab lab  balb</small>-->
		</div>
	</form>
</div>
<!-- FIM / Modal -  Complemento Chamado -->

	<!-- INICIO / Modal -  Solicita Posição Chamado -->
		<div id="manutChamadoSolicitaPosicao" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h1 class="muted">Solicitar Posição</h1>
		<p class="muted">Solicitar Posição do Chamado.</p>
		<table class="table-condensed">
				<tbody>
					<tr>
						<td> <strong> Usuário Atual: </strong> <br> <? echo strtoupper($this->session->userdata('user_login')); ?> </td>
						<td> <strong> Usuário Origem: </strong> <br> <? echo strtoupper($dados->row(0)->ID_USU_ORIGEM) ?> </td>
						<td> <strong> Data: </strong> <br> <? echo date("d/m/Y H:i:s"); ?></td>
					</tr>
				</tbody>
			</table>
		<hr>
		<div class="modal-body">
			<form method="POST" action="<? echo(site_url('c_uniupdt/insereComplemento/'.$dados->row(0)->NR_PROTOCOLO."/".$dados->row()->CPF)); ?>" class="form-search">
				<div class="input-append">
					<textarea  name="txtComplemento" id="txtComplemento" style="width:520px;" rows="10"> </textarea><br>
					<input type="hidden" name="tipoMovimento" value="03">
					<input type="hidden" name="usuOrigem" value="<? echo $dados->row(0)->ID_USU_ORIGEM ?>">
				</div>
		</div> 
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Solicitar Posição</button>
				<!--<small class="muted">Teste bla bla bla blab lab  balb</small>-->
			</div>
			</form>
</div>
<!-- FIM / Modal -  Solicita Posição Chamado -->

	<!-- INICIO / Modal -  Cancela Chamado -->
	<form method="POST" action="<? echo(site_url('c_uniupdt/cancelaChamado/'.$dados->row(0)->NR_PROTOCOLO."/".$dados->row(0)->CPF)); ?>" >
		<div id="manutChamadoCancela" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h1 class="muted">Cancela Chamado</h1>
		<table class="table-condensed">
				<tbody>
					<tr>
						<td> <strong> Usuário Atual: </strong> <br> <? echo strtoupper($this->session->userdata('user_login')); ?> </td>
						<td> <strong> Usuário Origem: </strong> <br> <? echo strtoupper($dados->row(0)->ID_USU_ORIGEM) ?> </td>
						<td> <strong> Data: </strong> <br> <? echo date("d/m/Y H:i:s"); ?></td>
					</tr>
				</tbody>
			</table>
		<hr>
		<div class="modal-body">
				<p> Informe qual o Motivo do Cancelamento: </p>
				<div class="input-append">
					<textarea  name="txtComplemento" id="txtComplemento" style="width:520px;" rows="10"> </textarea><br>
					<input type="hidden" name="tipoMovimento" value="07">
					<input type="hidden" name="usuOrigem" value="<? echo $dados->row(0)->ID_USU_ORIGEM ?>">
				</div>
		</div> 
			<div class="modal-footer">
				<input type="submit" class="btn btn-warning" value="Cancelar Chamado">
				<!--<small class="muted">Teste bla bla bla blab lab  balb</small>-->
			</div>
		</div>
	</form>
<!-- FIM / Modal -  Cancela Chamado -->