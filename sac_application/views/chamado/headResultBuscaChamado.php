<!-- Inicio do class registros -->
<div class="container">
	<h1>Resultado Busca de Chamados</h1>
		<p class="lead muted"><small> Utiliza o campo "Buscar" para resultar em buscas mais específicas.</small></p>
	<hr>

<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="bkcontrol">
	<thead>
		<tr>
			<th>Nº Chamado</th>
			<th>Resumo</th>
			<th>Abertura</th>
			<th>Status</th>
			<th>Motivo </th>
			<th>Ações </th>
		</tr>
	</thead>
<tbody>