<html>
  <head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['areaacao', 'volume de atendimento'],
          ['Intercâmbio',     <? echo('22')?>],
          ['Brasfels',      <? echo('43')?>],
          ['Cooperado',  <? echo('50')?> ]
        ]);

        var options = {
          title: 'Volume de Atendimento',
          is3D: true,
          // pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="piechart_3d" style="width: 700px; height: 400px;"></div>
  </body>
</html>