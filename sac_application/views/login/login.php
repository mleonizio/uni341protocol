    <div class="container">
      <div id="login" class="form-signin form-group has-success">
      <? if( $this->session->flashdata('no-logado') != "")
           {
            echo "<div class='alert-error alert fade in'>
                      <button type='button' class='close' data-dismiss='alert'>×</button>".$this->session->flashdata('no-logado')."</div>";
            }elseif( $this->session->flashdata('no-auth') != "")
           {
            echo "<div class='alert-error alert fade in text-center'>
                  <button type='button' class='close' data-dismiss='alert'>×</button>".$this->session->flashdata('no-auth')."</div>";
            }

            // COOKIES CONFIGURATIONS 

          if (!get_cookie('SAC/Remember-me')) {

            $checked = 'checked';

          }else{

            $checked = '';
          }
           $token = get_cookie('SAC/Remember-me', false);
          // COOKIES CONFIGURATIONS 

         ?>
        <h1>Protocolo de Atendimento<br>
          <small class="text-success">Unimed Angra dos Reis</small>
        </h1>
         <?  
        echo validation_errors();
        echo form_open('/c_auth'); ?>
          <?     
          $user = array (           
            'name'        => 'user',
            'id'          => 'inputSuccess',
            'type'        => 'text',
            'placeholder' => 'Usuário',
            'value'       =>  base64_decode($token),
            'class'       => 'form-control input-block-level '
          );
          $pass = array (           
            'name'        => 'pass',
            'id'          => 'pass',
            'type'        => 'password',
            'placeholder' => 'Senha',
            'class'       => 'form-control input-block-level'
          );  

          $checkbox = array (           
            'name'        => 'remember',
            'type'        => 'checkbox',
            'value'       => 1,
            $checked      => $checked
          );   

          $button = array (
            'value'       => 'ENTRAR',
            'class'       => 'btn btn-large btn-success'
          );

         ?>

        <? echo form_input($user) ?>
        <? echo form_input($pass) ?>
        <label class="checkbox">
        <? echo form_input($checkbox) ?>  Lembrar Usuário
        </label>
        <? echo form_submit($button)?>
        <? echo form_close(); ?>

      </div>
    </div>
  