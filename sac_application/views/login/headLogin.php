	<? 

	/**
	*
	*
	* Central de atendimento ao cliente - Unimed Angra dos Reis
	* @author Marcelo Leonízio - eu@marceloleonizio.com
	*
	*
	**/

	echo doctype('html5');
	?>
<html lang="pt-br">
<head>
	<?
	$meta = array(
	 array('name' => 'description', 'content' => 'Serviço de atendimento ao consumidor - Unimed Angra dos Reis'),
	 array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv'),
	 array('name' => 'viewport','content' => 'width=device-width, initial-scale=1'),
	 array('name' => 'author','content' => 'Marcelo Leonizio - @mleonizio')
  	);	
	echo meta($meta);
	?>

	<title>Central de Atendimento ao Cliente - Unimed Angra dos Reis</title>
	<? echo link_tag('assets/css/bootstrap.css'); ?>
	<? echo link_tag('assets/css/login.css'); ?>
	<? echo link_tag('assets/css/bootstrap-responsive.css'); ?>
	<? echo link_tag('assets/css/bootstrap-theme.css'); ?>
	<? echo link_tag('assets/css/font-awesome.min.css'); ?>
	<? //echo link_tag('assets/css/canvas.css'); ?>
	<? //echo link_tag('assets/css/util.css')?>

	 <link rel="shortcut icon" href="<? echo base_url('assets/img/favicon.ico')?>" />

</head>
	<body>