<?
					echo validation_errors();
			  		echo form_open('c_unicentral/atualizaCadastro') . "\n\n";

			  		//  Dados Pessoais ----------------------------------------

					$nomeCliente = array(					 
					  'name'		=> 'nopessoa',
					  'type'		=>	'text',
					  'value'		=>  $nopessoa,
					  'class'		=>	'uneditable-input',
					  'disabled'	=>	'disabled',
					  'autocomplete'=>	'off',
					  'style'       =>  'width:200px; height:30px;',
          );
          $dataNascimento = array(					 
					  'name'		=> 'dtnascimento',
					  'type'		=>	'text',
					  'value'		=>  $dtnascimento,
					  'class'		=>	'uneditable-input',
					  'disabled'	=>	'disabled',					  
					  'autocomplete'=>	'off',
					  'style'       => 'width:200px; height:30px;',
          );
          $idadeCliente = array(					 
					  'name'		=> 'idade',
					  'type'		=>	'text',
					  'value'		=>  $idade,
					  'class'		=>	'uneditable-input',
					  'disabled'	=>	'disabled',					  
					  'autocomplete'=>	'off',
					  'style'       => 'width:100px; height:30px;',
          );
          $nomeMae = array(					 
					  'name'		=> 'nomemae',
					  'type'		=>	'text',
					  'value'		=>  $nomemae,
					  'autocomplete'=>	'off',
					  'style'       => 'width:210px; height:30px;',
          );
          $estadoCivil = array(					 
					  'name'		=> 'noestadocivil',
					  'type'		=>	'text',
					  'value'		=>  $noestadocivil,
					  'autocomplete'=>	'off',
					  'style'       => 'width:200px; height:30px;',
          );
          $tipoUsuario = array(					 
					  'name'		=> 'meiocontato',
					  'type'		=>	'text',
					  'value'		=>  $notpusuario,
					  'class'		=>	'uneditable-input',
					  'disabled'	=>	'disabled',					  
					  'autocomplete'=>	'off',
					  'style'       => 'width:100px; height:30px;',
          );
          $numCpf = array(					 
					  'name'		=> 'nrcpf',
					  'type'		=>	'text',
					  'value'		=>  $nrcpf,
					  'autocomplete'=>	'off',
					  'style'       => 'width:210px; height:30px;',
          );
          $telContato = array(					 
					  'name'		=> 'meiocontato',
					  'type'		=>	'text',
					  'value'		=>  $meiocontato,
					  'autocomplete'=>	'off',
					  'style'       => 'width:200px; height:30px;',
          );

           			// Dados Pessoais -------------------------------------------

          $nomeCidade = array(					 
					  'name'		=> 'nocidade',
					  'type'		=>	'text',
					  'value'		=>  $nocidade,
					  'autocomplete'=>	'off',
					  'style'       => 'width:200px; height:30px;',
          );
          $codEstado = array(					 
					  'name'		=> 'cdestado',
					  'type'		=>	'text',
					  'value'		=>  $cdestado,
					  'autocomplete'=>	'off',
					  'style'       => 'width:100px; height:30px;',
          );
          $numCep = array(					 
					  'name'		=> 'nrcep',
					  'type'		=>	'text',
					  'value'		=>  $nrcep,
					  'autocomplete'=>	'off',
					  'style'       => 'width:200px; height:30px;',
          );
          $nomeBairro = array(					 
					  'name'		=> 'nobairro',
					  'type'		=>	'text',
					  'value'		=>  $nobairro,
					  'autocomplete'=>	'off',
					  'style'       => 'width:200px; height:30px;',
          );
          $nomeRua = array(					 
					  'name'		=> 'nologradouro',
					  'type'		=>	'text',
					  'value'		=>  $nologradouro,
					  'autocomplete'=>	'off',
					  'style'       => 'width:200px; height:30px;',
          );
  
            			// Dados do Plano -------------------------------------------

          $numContrato = array(					 
					  'name'		=> 'nrcontrato',
					  'type'		=>	'text',
					  'value'		=>  $nrcontrato,
					  'class'		=>	'uneditable-input',
					  'disabled'	=>	'disabled',					  
					  'autocomplete'=>	'off',
					  'style'       => 'width:100px; height:30px;',
          );
          $numFamilia = array(					 
					  'name'		=> 'nrfamilia',
					  'type'		=>	'text',
					  'value'		=>  $nrfamilia,
					  'class'		=>	'uneditable-input',
					  'disabled'	=>	'disabled',					  
					  'autocomplete'=>	'off',
					  'style'       => 'width:100px; height:30px;',
          );
          $tipoAcomodacao = array(					 
					  'name'		=> 'noacomodacao',
					  'type'		=>	'text',
					  'value'		=>  $noacomodacao,
					  'class'		=>	'uneditable-input',
					  'disabled'	=>	'disabled',					  
					  'autocomplete'=>	'off',
					  'style'       => 'width:200px; height:30px;',
          );
          $tipoCoparticipacao = array(					 
					  'name'		=> 'notpcoparticipacao',
					  'type'		=>	'text',
					  'value'		=>  $notpcoparticipacao,
					  'class'		=>	'uneditable-input',
					  'disabled'	=>	'disabled',					  
					  'autocomplete'=>	'off',
					  'style'       => 'width:200px; height:30px;',
          );
          $areaAcao = array(					 
					  'name'		=> 'areacao_unimed',
					  'type'		=>	'text',
					  'value'		=>  $areacao_unimed,
					  'class'		=>	'uneditable-input',
					  'disabled'	=>	'disabled',					  
					  'autocomplete'=>	'off',
					  'style'       => 'width:200px; height:30px;',
          );
          $dataInicio = array(					 
					  'name'		=> 'dtinicio',
					  'type'		=>	'text',
					  'value'		=>  $dtinicio,
					  'class'		=>	'uneditable-input',
					  'disabled'	=>	'disabled',					  
					  'autocomplete'=>	'off',
					  'style'       => 'width:100px; height:30px;',
          );
          $abrangencia = array(					 
					  'name'		=> 'noabrangencia',
					  'type'		=>	'text',
					  'value'		=>  $noabrangencia,
					  'class'		=>	'uneditable-input',
					  'disabled'	=>	'disabled',					  
					  'autocomplete'=>	'off',
					  'style'       => 'width:100px; height:30px;',
          );
?>
<div class="container">
	<div>
		<h1>Atualizar dados de cadastro</h1>
		<p class="lead muted"><small> Utilize o capo "Buscar" para ser mais específico ao procurar por um nome em particular</small></p>
		<hr>
	</div>
<div class="span3">
	<div class="well">
		<h2 class="muted">Dados Relevantes</h2>
		<div class="well">			
			Ocorrências Abertos: 	<span class="label label-important pull-right"><? echo $aberto ?></span>
			Ocorrências Pendentes: 	<span class="label label-warning pull-right"><? echo $pendente ?></span>
			Ocorrências Fechadas:	<span class="label label-success pull-right"><? echo $fechado ?></span>
		</div>
		
		<h3 class="text-warning">Abrir uma Ocorrência</h3>
		<hr>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta.</p>
		<hr>
		<p><a disabled="disabled" data-confirm="Antes, será feita uma verificação no usuário, para não haver ocorrências repetidas abertas, caso contrário a ocorrência será aberta com sucesso." class="btn btn-large btn-danger" href="<? echo(site_url('c_unicentral/confereDadosUsuOcorrencia'.'/'.$cdcgc_cpf))?>" ><i class="icon-ok"></i> Abrir Ocorrência</a></p>
	</div>
</div>
<div class="span1"> </div>

<div class="container">
	<? echo form_fieldset("Dados Pessoais")?>
	<div class="container-fluid">
  <div class="row-fluid">
    <div class="span4">
      <? 
      echo form_label('Nome Cliente:');
      echo form_input($nomeCliente);
      ?>
    </div>
    <div class="span4">
      <? 
      echo form_label('Data Nascimento:');
      echo form_input($dataNascimento);
      ?>
    </div>
    <div class="span4">
         <? 
      echo form_label('Idade Atual:');
      echo form_input($idadeCliente);
      ?>
    </div>
  </div> 
</div>
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span4">
      <? 
      echo form_label('Nome da Mãe:');
      echo form_input($nomeMae);
      ?>
    </div>
    <div class="span4">
      <? 
      echo form_label('Estado Civil:');
      echo form_input($estadoCivil);
      ?>
    </div>
    <div class="span4">
         <? 
      echo form_label('Tipo de Usuário:');
      echo form_input($tipoUsuario);
      ?>
    </div>
  </div> 
</div>
 
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span4">
      <? 
      echo form_label('CPF:');
      echo form_input($numCpf);
      ?>
    </div>
    <div class="span4">
      <? 
      echo form_label('Telefone Contato:');
      echo form_input($telContato);
      ?>
    </div>
  </div> 
</div>
 
<br>
 
<? echo form_fieldset("Dados Cadastrais")?>
	<div class="container-fluid">
  <div class="row-fluid">
    <div class="span4">
      <? 
      echo form_label('Cidade:');
      echo form_input($nomeCidade);
      ?>
    </div>
    <div class="span4">
      <? 
      echo form_label('Bairro:');
      echo form_input($nomeBairro);
      ?>
    </div>
    <div class="span4">
         <? 
      echo form_label('UF:');
      echo form_input($codEstado);
      ?>
    </div>
  </div> 
</div>
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span4">
      <? 
      echo form_label('CEP:');
      echo form_input($numCep);
      ?>
    </div>
    <div class="span4">
      <? 
      echo form_label('Rua:');
      echo form_input($nomeRua);
      ?>
    </div>
  </div> 
</div>

<br>
 
<? echo form_fieldset("Dados do Plano")?>
	<div class="container-fluid">
  <div class="row-fluid">
    <div class="span4">
      <? 
      echo form_label('Numero Contrato:');
      echo form_input($numContrato);
      ?>
    </div>
    <div class="span4">
      <? 
      echo form_label('Família:');
      echo form_input($numFamilia);
      ?>
    </div>
    <div class="span4">
         <? 
      echo form_label('Tipo de Acomodação:');
      echo form_input($tipoAcomodacao);
      ?>
    </div>
  </div> 
</div>
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span4">
      <? 
      echo form_label('Tipo de Co-participação:');
      echo form_input($tipoCoparticipacao);
      ?>
    </div>
    <div class="span4">
      <? 
      echo form_label('Area de Ação do Plano:');
      echo form_input($areaAcao);
      ?>
    </div>
     <div class="span4">
      <? 
      echo form_label('Cliente desde:');
      echo form_input($dataInicio);
      ?>
    </div>
  </div> 
</div>
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span4">
      <? 
      echo form_label('Abrangência do Plano:');
      echo form_input($abrangencia);
      ?>
    </div>
  </div> 
</div>
 
<br>
 
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span4 pull-right">
    	<hr>
      <button class="btn btn-large btn-success" style="width:220px; height:40px;" data-loading-text="Enviando...." disabled="disabled" > Atualizar Dados </button>     
    </div>
  </div>
</div>
</div>