<?
//      Verifica usuario logado, se não redireciona para o login
     if( !$this->session->userdata('user_logado') )
    {
      $this->session->set_flashdata('no-logado', 'Desculpe, você precisa fazer o login para ver essa página.');
      redirect('/c_auth', 'refresh');     
    }
//   ### FIM ###   Verifica usuario logado, se não redireciona para o login

  
  if(@$_GET['cd_cliente']){

           $buscaCodCliente = array(          
            'name'              => 'cd_cliente',
            'type'              => 'text',
            'id'                => 'buscaCdCliente',
            'maxlength'         => '18',
            'onkeypress'        => 'return formatInputBusca(this)',
            'onchange'          => 'javascript:buscaId()',
            'autocomplete'      => 'off',
            'value'             =>  $areaacao.".".$nrcontrato.".".str_pad($nrfamilia,6,'0', STR_PAD_LEFT).".".$tpusu,
            'style'             => 'width:500px; height:20px'
          );           

          $buscaNomeCliente = array(          
            'name'              => 'cd_cliente',
            'type'              => 'text',
            'autocomplete'      =>  'off',
            'style'             => 'width:500px; height:20px'
          );

          $nomeCliente = array(
            'name'              => 'nopessoa',
            'type'              => 'text',
            'value'             =>  $nomeCliente,
            'autocomplete'      =>  'off',
            
            );

          $meioContato = array(          
            'name'              => 'meiocontato',
            'type'              => 'text',
            'value'             => $meiocontato,
            'autocomplete'      => 'off',

            );

          $cgc_cpf = array(
            'name'              => 'cdcgc_cpf',
            'value'             => $cgc_cpf,
            'type'              => 'text'
          );

  }else{
          $buscaCodCliente = array(          
            'name'              => 'cd_cliente',
            'id'                => 'buscaCdCliente',
            'maxlength'         => '18',
            'onkeypress'        => 'return formatInputBusca(this)',
            'onchange'          => 'javascript:buscaId()',
            'type'              => 'text',
            'autocomplete'      => 'off',
            'style'             => 'width:500px; height:20px'

          );
          $buscaNomeCliente = array( 

            'name'              => 'cd_cliente',
            'onchange'          => 'javascript:buscaId()',
            'type'              => 'text',
            'autocomplete'      => 'off',
            'style'             => 'width:500px; height:20px'
          );

          $nomeCliente = array(
            'name'              => 'nopessoa',
            'type'              => 'text',
            'autocomplete'      => 'off',
            
            );

          $meioContato = array(    

            'name'              => 'meiocontato',
            'type'              =>  'text',
            'autocomplete'      =>  'off',
            
          );

          $cgc_cpf = array(
            'name'              => 'cdcgc_cpf',
            'type'              => 'text'
          );
    }


         $historicoSac = array(   

            'name'              =>  'historico_sac',
            'class'             =>  'ckeditor',
          );

          $dataLimite = array(

            'name'              => 'data_limite', 
            'id'                => 'dataLimite',
            'type'              => 'text'
          );

          $btn_enviar = array(

            'class'             => 'btn btn-large btn-danger',
            'style'             => 'width:220px; height:40px',
            'value'             => 'Abrir Chamado'
          );             
?>

<div class="container">
<?

      if( $this->session->flashdata('carteirinha') != "")
      {
        echo "<div class='alert-error alert fade in'>
                  <button type='button' class='close' data-dismiss='alert'>×</button>".$this->session->flashdata('carteirinha')."</div>";
      }

  if ( $this->session->flashdata('abreOcorrencia-ok') != "")
          {
            echo "<div class='alert-success alert fade in'>
                      <button type='button' class='close' data-dismiss='alert'>×</button>".$this->session->flashdata('abreOcorrencia-ok')."</div>"; 
          }
          
  // Menssagens do servidor
  
        if ( $this->session->flashdata('no-existe') != "")
      {
        echo "<div class='alert-warning alert fade in'>
                  <button type='button' class='close' data-dismiss='alert'>×</button>".$this->session->flashdata('no-existe')."</div>"; 
      }
?>


  
    <h1>Novo Chamado</h1>
    <p class="lead muted"><small> Gerar um novo atendimento com número de protocolo</small></p>
    <hr>

<!-- #### CHAMADO POR CODIGO #### -->
<?  
echo validation_errors();
$atributos = array('name' => 'chamadoForm');
echo form_open('c_unicentral/confereDadosUsuOcorrencia', $atributos) . "\n\n";
?>
    <div class="container-fluid span9">
      <div class="row-fluid">
        <div class="span8">
                <? echo form_label('Entre com o Código da carteirinha:'); ?>
                    <div class="input-append">
                     <? echo form_input($buscaCodCliente); ?>
                         <a href="#floatPage" role="button"  class="btn" data-toggle="modal" title="Buscar Clientes Unimed Angra" > <i class="icon-search"></i></a>
                   </div> 
                     <!-- DIV VALIDACAO CAMPO #buscaCodCliente -->
                       <div id="validacaoCampoJs"></div>
             </div>
         </div>  
      </div>


<!-- #### CHAMADO POR NOME #### -->
<!-- <form action="" id="cdCarteirinha">
    <div class="container-fluid span9">
      <div class="row-fluid">
        <div class="span8">
                <? //echo form_label('Entre com o Nome do Cliente:'); ?>
                     <? //echo form_input($buscaNomeCliente); ?>
             </div>
         </div>  
    </div>
</form> --> 

 <hr class="span6">
 
 <!-- #############  -->

<div class="container-fluid span9">
  <div class="row-fluid">
    <div class="span4">
      <? 
      echo form_label('Nome Cliente:');
      echo form_input($nomeCliente);
      ?>
    </div>
    <div class="span5">
    <?
       echo form_label('Contato Cliente:');
       echo form_input($meioContato);
    ?>
    </div>
  </div> 
</div>

<div class="container-fluid span9">
  <div class="row-fluid">
  <div class="span4">
      <?
         echo form_label('Designar Setor específico:');
      ?>
    <select name="setor">
    <option></option>
    <? 
        foreach ($setor->result_array() as $set) {
          echo "<option value='$set[CD_SETOR]'> $set[NO_SETOR] </option>";
           }
    ?>
    </select>
    </div>
  <!-- <div class="span4">
      <? 
      //echo form_label('Categoria');
      ?>
      <select name="categoria" id="categoria">
            <option>Escolha a categoria</option>
            <? 
                // foreach ($categoria->result_array() as $cat) {
                 // echo "<option value='$cat[CD_CATEGORIA]'> $cat[NO_MOTIVO_CATEGORIA] </option>";
              // }
            ?>
     </select> 
    </div> -->

    <div class="span5">
      <? 
      echo form_label('Motivo da abertura:');
      ?>
      <select name="diaLimiteMotivo" id="diaLimite" onchange="mostraDia(chamadoForm)">
      <option value=""></option>}
            <? 

                foreach ($motivo->result_array() as $mot) {
                 echo "<option id='$mot[DIAS_LIMITE]' value='$mot[DIAS_LIMITE]'> $mot[NO_MOTIVO_SAC] </option>";
            }
          ?>
     </select> 
      
     
    </div>
  </div>  
</div>

<div class="container-fluid span9">
  <div class="row-fluid add-on">
      <div class="span4">
      <?
        echo form_label('CPF / CNPJ');
        echo form_input($cgc_cpf);
      ?>
    </div>
  <!-- <div class="span5">
    <? //echo form_label('Prioriade do Chamado');?>
     <select name="prioridade">
       <option value="3" selected="selected">Baixa</option>
       <option value="2">Média</option>
       <option value="1">Alta</option>
     </select>
    </div> -->
  </div>  
</div>

<div class="container-fluid span9">
  <div class="row-fluid">
    <div class="span4">
      <? 
      echo form_label('Protocolo de Origem:');
      echo form_input();
      ?>
    </div>
    <div class="span4">
    <?
       echo form_label('Data limite para tendimento');
       echo form_input($dataLimite);
    ?>
    </div>
  </div> 
</div>

<div class="container-fluid span9">
  <div class="row-fluid">
    <div class="span8">
      <? 
      echo form_label('Descrição Ocorrência:');
      echo form_textarea($historicoSac);
      ?>     
    </div>
  </div>  
  <div class="input-block-level"> </div>
</div>
 
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span8">
      <div class="span4 offset6">
      <hr>
        <?
          echo form_submit($btn_enviar);
          echo form_close();
        ?>
        <!-- <button class="btn btn-large btn-danger" style="width:220px; height:40px;" data-loading-text="Enviando...." > Abrir Chamado </button>      -->
      </div>
    </div>
  </div>
</div>
</div>

    
<!-- // JAVA SCRIPT -->

<script type="text/javascript">
jQuery(function($){
   //$(".teste").mask("999.9999.999999.99",{placeholder:"0"});
   // $("#cd_intercambio").mask("999.9999.999999.99",{placeholder:"0"});
});

</script>