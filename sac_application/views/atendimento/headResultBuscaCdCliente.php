<!-- Inicio do class registros -->
<div class="container">
	<h1>Resultado Busca Clientes</h1>
		<p class="lead muted"><small> Utiliza o campo "Buscar" para resultar em buscas mais específicas, caso haja mais de um resultado.</small></p>
	<hr>

<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="bkcontrol">
	<thead>
		<tr>
			<th>Nome Cliente</th>
			<th>Número do Contrato</th>
			<th>Número da Família </th>
			<th>Tipo de Usuário </th>
			<th class="text-info"><i class="icon-tasks"> | </i>Ação </th>
		</tr>
	</thead>
<tbody>