<?
//     	Verifica usuario logado, se não redireciona para o login
	   if( !$this->session->userdata('user_logado') )
		{
			$this->session->set_flashdata('no-logado', 'O tempo da sessão expirou (timeout), faça login novamente.');
			redirect('/c_auth', 'refresh');			
		}
?>
	<div class="container">

 <?    if ( $this->session->flashdata('encerra-chamado') != "")
          {
            echo "<div class='success alert fade in'>
                      <button type='button' class='close' data-dismiss='alert'>×</button>".$this->session->flashdata('encerra-chamado')."</div>"; 
          }else if ( $this->session->flashdata('cancela-chamado') != "")
          {
            echo "<div class='success alert fade in'>
                      <button type='button' class='close' data-dismiss='alert'>×</button>".$this->session->flashdata('cancela-chamado')."</div>"; 
          }
 ?>
      <div class="hero-unit">
        <!--  <i class='span3'><img src="<? //echo base_url('assets/img/unimed-logo.png');?>"></i> -->
        <h2>Bem Vindo, <em><? echo $this->session->userdata('user_id')?></em><br><span class="text-success">Unimed Angra dos Reis <br> <? echo $this->session->userdata('cd_fun')?></span></h2>
      </div>
      

      <div class="input-block-level"></div>

      <div class="row">
        <div class="span4">
          <div class="well well-large">
          	<h4 class="text-success">Atualizar dados de Clientes</h4>
            <p>Atualização de dados de cadastro dos clientes Unimed Angra.</p>
              <a href="#floatPage" role="button" class="btn btn-success" data-toggle="modal"><i class="icon-plus-sign-alt"> | </i> Atualizar Dados</a>
          </div>
        </div>
    	<div class="span4">
          <div class="well well-large">
            <h4 class="text-success">Histórico</h4>
            <p>Detalhes de protocolos já abertos, históricos de protocolos.</p>
            <a href="#" role="button" class="btn btn-success" data-toggle="modal"> <i class="icon-list-ol"> | </i>Detalhe Histórico</a>
         </div>
        </div>
	   	<div class="span4">
           <div class="well well-large">
            <h4 class="text-error"><strong> Chamados com Alta Prioridade</strong> <span class="label label-important">05</span> </h4>
            <p class="text-error">Atendimentos abertos, com prioridade máxima na execução.</p>
            <a href="<? echo(site_url('c_unicentral/novoAtendimento')) ?>" class="btn btn-danger" data-toggle="modal"> <i class="icon-exclamation-sign"> | </i>Alta Prioridade </a>
         </div>
         </div>
      </div>
   </div> <!-- /Fim div.container -->