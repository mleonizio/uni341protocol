<div class="navbar navbar-inverse navbar-fixed-top nav-list">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<? echo(site_url('c_unicentral/')); ?>"><img src="<? echo base_url('assets/img/unimed-brand.png');?>"></a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li><a href="<? echo(site_url('c_unicentral/geraAtendimento')) ?>" role="button" data-toggle="modal"><i class="icon-plus-sign-alt"> | </i> Abrir Chamado</a></li>
              <li><a href="#floatPageManutencao" role="button" data-toggle="modal"> <i class="fa fa-stack-exchange"> | </i> Manut. do Chamado</a></li>
              <li><a href="<? echo(site_url('c_unicentral/detalheHistorico')); ?>"> <i class="icon-list-ol"> | </i> Hisitórico</a></li>
              <li><a href="<? echo(site_url('c_unicentral/help')); ?>"><i class="icon-question-sign"> | </i>  Ajuda</a> </li>
            
              <li class="pull-right">
                <div class="btn-group">
                  <a class="btn btn-success" href="#"><i class="icon-user icon-white"></i> <? echo 'Olá, <strong>'. strtoupper($this->session->userdata('user_login'))."</strong>"?></a>
                  <a class="btn btn-success dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                    <li><a tabindex="-1" href="<? echo(site_url('c_unicentral/sugestaoUsuario')); ?>"><i class="icon-comment"> | </i> Sugestões </a></li>
                    <li class="divider"></li>
                    <li><a tabindex="-1" href="<? echo(site_url('c_unicentral/logout')); ?>"><i class="icon-ban-circle"> | </i> Sair</a></li>
                  </ul>
                </div>
              </li>
            </ul>
          </div><!-- nav-collapse -->
        </div>
      </div>
    </div>
    
 <!-- INICIO / Modal -  Página para buscar cliente -->
    <div id="floatPage" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h1 class="text-success">Buscar Cliente</h1>
    <p class="muted">Preferêncialmente digite o nome e sobre nome do cliente, assim a busca fica mais específica.</p>
    <hr>
    <div class="modal-body">
      <form method="GET" action="<? echo(site_url('c_unicentral/c_getNomeCliente/')); ?>" class="form-search">
        <div class="input-append">
          <input type="text" name="queryNome" class="span2" id="appendedInputButton" placeholder="Nome Cliente" style="width:500px; heigth:20px">
            <button type="submit" class="btn btn-success"><i class="icon-search"></i></button>
        </div>
        </form>
    </div> 
  <div class="modal-footer">
      <button class="btn btn-success" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"> | </i>Fechar Janela</button>
  </div>
</div>
<!-- FIM / Modal -  Página para buscar cliente -->

 <!-- INICIO / Modal -  Página para buscar cliente por CARTEIRINHA -->
    <div id="floatPageCarteirinha" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h1 class="text-success">Buscar Cliente Unimed</h1>
    <p class="muted">Entre com o código da carteirinha.</p>
    <hr>
    <div class="modal-body">
      <form method="GET" action="<? echo(site_url('c_unicentral/c_getCdCliente/')); ?>" class="form-search">
        <div class="input-append">
          <input type="text" name="cd_cliente" id="cd_cliente" class="span5" autocomplete="off">
            <button type="submit" class="btn btn-success"><i class="icon-search"></i></button>
        </div>
        </form>
    </div> 
  <div class="modal-footer">
      <button class="btn btn-success" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"> | </i>Fechar Janela</button>
  </div>
</div>
<!-- FIM / Modal -  Página para buscar cliente por CARTEIRINHA -->

 <!-- INICIO / Modal -  Página para MANUTENCAO do chamado -->
    <div id="floatPageManutencao" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h1 class="text-info">Selecionar Chamados</h1>
    <p class="muted">Escolha o tipo de Busca.</p>
    <hr>
    <div class="modal-body">
      <form method="GET" action="<? echo(site_url('c_unicentral/resultadoBuscaChamado/')); ?>" class="form-search">
       <div class="container-fluid">
        <div class="row-fluid">
        <div class="span2">
          <label class="checkbox"><input type="checkbox" name="tipo" value="4"> Todos </label><br><br>
        </div>
        <div class="span3">
          <label class="checkbox"><input type="checkbox" name="tipo" value="1"> Abertos </label><br>
          <label class="checkbox"><input type="checkbox" name="tipo" value="2"> Fechados </label><br>
        </div>

        <div class="span3">
          <label class="checkbox"><input type="checkbox" name="tipo" value="0"> Sem Atendentes </label><br>
          <label class="checkbox"><input type="checkbox" name="tipo" value="3"> Cancelados </label><br><br>
        </div>
        
        <button type="submit" class="btn btn-large btn-info"><i class="icon-search"></i> Procurar </button>
        
        </div>
        </div>
        </form>
    </div> 
  <div class="modal-footer">
      <!-- <button class="btn btn-success" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"> | </i>Fechar Janela</button> -->
  </div>
</div>
<!-- FIM / Modal -  Página para MANUTENCAO do chamado -->
