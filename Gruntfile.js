module.exports = function (grunt) {
	'use strict';
	 
    grunt.initConfig({
    	
    	pkg: grunt.file.read('package.json'),
    	watch: {

        css: {
          files: 'assets/css/*.css',
            options: {
              livereload: true,
                },
             },

        scripts: {
            files: ['assets/js/*.js', 'assets/js/*/*.js'],
            tasks: ['jshint'],
              },

        controllers: {
            files: ['sac_application/controllers/*.php', 'sac_application/controllers/*/*.php'],
             options: {
              livereload: true,
                },
            },
        models: {
            files: ['sac_application/models/*.php', 'sac_application/models/*/*.php'],
             options: {
              livereload: true,
                },
            },
        views: {
            files: ['sac_application/views/*.php', 'sac_application/views/*/*.php'],
             options: {
              livereload: true,
                },
          }
            },
    });

	
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-livereload');
  grunt.loadNpmTasks('grunt-browser-sync');

	grunt.registerTask('default', ['watch','jshint']);

};