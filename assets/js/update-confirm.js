function confirm(heading, question, cancelButtonTxt, okButtonTxt, callback) {
  
    var updateModal = 
      $('<div class="modal hide fade">' +    
          '<div class="modal-header">' +
            '<a class="close" data-dismiss="modal" >&times;</a>' +
            '<h3>' + heading +'</h3>' +
          '</div>' +

          '<div class="modal-body">' +
            '<p>' + question + '</p>' +
          '</div>' +

          '<div class="modal-footer">' +
            '<a href="#" class="btn" data-dismiss="modal">' + 
              cancelButtonTxt + 
            '</a>' +
            '<a href="#" id="okButton" class="btn btn-primary">' + 
              okButtonTxt + 
            '</a>' +
          '</div>' +
        '</div>');

    updateModal.find('#okButton').click(function(event) {
      callback();
      update.modal('hide');
    });

    updateModal.modal('show');     
  };


  $(document).ready(function() {
    $('a[data-update]').click(function(ev) {
        var href = $(this).attr('href');

        if (!$('#dataupdateModal').length) {
            $('body').append('<div id="dataupdateModal" class="modal hide fade" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3 id="dataConfirmLabel">Atenção </h3></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button><a class="btn btn-primary" id="dataConfirmOK">Continuar</a></div></div>');
        } 
        $('#dataupdateModal').find('.modal-body').text($(this).attr('data-update'));
        $('#dataupdateOK').attr('href', href);
        $('#dataupdateModal').modal({show:true});
        return false;
    });
});