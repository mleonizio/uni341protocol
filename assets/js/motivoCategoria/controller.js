$(document).ready(function(){
   $("#categoria").change(function(){
      $.ajax({
         type: "POST",
         url: "exemplo.php",
         data: {montadora: $("#categoria").val()},
         dataType: "json",
         success: function(json){
            var options = "";
            $.each(json, function(key, value){
               options += '<option value="' + key + '">' + value + '</option>';
            });
            $("#diaLimite").html(options);
         }
      });
   });
});