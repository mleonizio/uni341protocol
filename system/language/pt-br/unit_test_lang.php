<?php

$lang['ut_test_name']		= 'Teste Nome';
$lang['ut_test_datatype']	= 'Teste Datatype';
$lang['ut_res_datatype']	= 'É Experado um Datatype';
$lang['ut_result']			= 'Resultado';
$lang['ut_undefined']		= 'Teste nome não foi definido';
$lang['ut_file']			= 'Nome do Arquivo';
$lang['ut_line']			= 'Número da Linha';
$lang['ut_passed']			= 'Passou';
$lang['ut_failed']			= 'Falhou';
$lang['ut_boolean']			= 'Booleano';
$lang['ut_integer']			= 'Inteiro';
$lang['ut_float']			= 'Float';
$lang['ut_double']			= 'Float'; // pode ser o mesmo, como float
$lang['ut_string']			= 'String';
$lang['ut_array']			= 'Array';
$lang['ut_object']			= 'Objeto';
$lang['ut_resource']		= 'Recurso';
$lang['ut_null']			= 'Nulo';
$lang['ut_notes']			= 'Notas';


/* End of file unit_test_lang.php */
/* Translate Pt-br by: Marcelo Leonízio - @mleonizio */ 
/* Location: ./system/language/english/unit_test_lang.php */