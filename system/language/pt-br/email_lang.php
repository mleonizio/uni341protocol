<?php

$lang['email_must_be_array'] = "O método de validação de e-mail deve ser passado um array.";
$lang['email_invalid_address'] = "Endereço de e-mail inválido: %s";
$lang['email_attachment_missing'] = "Não foi possível localizar o seguinte anexo de e-mail: %s";
$lang['email_attachment_unreadable'] = "Não é possível abrir este anexo: %s";
$lang['email_no_recipients'] = "Você deve incluir destinatários: Para, Cc ou Bcc";
$lang['email_send_failure_phpmail'] = "Não é possível enviar e-mail usando PHP mail (). O servidor não pode ser configurado para enviar e-mail utilizando este método.";
$lang['email_send_failure_sendmail'] = "Não é possível enviar e-mail usando PHP Sendmail. O servidor não pode ser configurado para enviar e-mail utilizando este método.";
$lang['email_send_failure_smtp'] = "Não é possível enviar e-mail usando PHP SMTP. O servidor não pode ser configurado para enviar e-mail utilizando este método.";
$lang['email_sent'] = "Sua mensagem foi enviada com sucesso usando o protocolo a seguir: %s";
$lang['email_no_socket'] = "Não é possível abrir um soquete para Sendmail. Por favor, verifique as configurações.";
$lang['email_no_hostname'] = "Você não especificou um SMTP hostname.";
$lang['email_smtp_error'] = "O seguinte erro SMTP foi encontrado: %s";
$lang['email_no_smtp_unpw'] = "Erro: Você deve atribuir um nome de usuário e senha SMTP.";
$lang['email_failed_smtp_login'] = "Falha ao enviar o comando LOGIN AUTH. Erro: %s";
$lang['email_smtp_auth_un'] = "Falhou a autenticação de usuário. Erro: %s";
$lang['email_smtp_auth_pw'] = "Falha na autenticação de senha. Erro: %s";
$lang['email_smtp_data_failure'] = "Não é possível enviar os dados: %s";
$lang['email_exit_status'] = "Código de status de saída: %s";


/* End of file email_lang.php */
/* Translate Pt-br by: Marcelo Leonízio - @mleonizio */ 
/* Location: ./system/language/english/email_lang.php */