<?php

$lang['profiler_database']		= 'BANCO DE DADOS';
$lang['profiler_controller_info'] = 'CLASSE/MÉTODO';
$lang['profiler_benchmarks']	= 'PERFORMANCE';
$lang['profiler_queries']		= 'QUERIES';
$lang['profiler_get_data']		= 'GET DATA';
$lang['profiler_post_data']		= 'POST DATA';
$lang['profiler_uri_string']	= 'URI STRING';
$lang['profiler_memory_usage']	= 'USO DA MEMÓRIA';
$lang['profiler_config']		= 'CONFIG VARIABLES';
$lang['profiler_session_data']	= 'SESSION DATA';
$lang['profiler_headers']		= 'HTTP HEADERS';
$lang['profiler_no_db']			= 'Driver de banco de dados não está carregado atualmente';
$lang['profiler_no_queries']	= 'Nenhuma querie está sendo executada';
$lang['profiler_no_post']		= 'Não existe dados de POST';
$lang['profiler_no_get']		= 'Não existe dados de GET';
$lang['profiler_no_uri']		= 'Não existe dados de URI';
$lang['profiler_no_memory']		= 'Uso de memória indisponível';
$lang['profiler_no_profiles']	= 'Não ha dados de Perfil - Todoas as seções foram desabilitadas.';
$lang['profiler_section_hide']	= 'Hide';
$lang['profiler_section_show']	= 'Show';

/* End of file profiler_lang.php */
/* Translate Pt-br by: Marcelo Leonízio - @mleonizio */ 
/* Location: ./system/language/english/profiler_lang.php */