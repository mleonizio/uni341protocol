<?php

$lang['migration_none_found']			= "Não foram encontradas migrações.";
$lang['migration_not_found']			= "Esta migração não pôde ser encontrada.";
$lang['migration_multiple_version']		= "Existe várias migrações com o mesmo número de versão: %d.";
$lang['migration_class_doesnt_exist']	= "A classe migração \"%s\" não pôde ser encontrada.";
$lang['migration_missing_up_method']	= "A classe migração \"%s\" está faltando um método 'up'.";
$lang['migration_missing_down_method']	= "A classe migração \"%s\" está faltando um método 'down'.";
$lang['migration_invalid_filename']		= "Migration \"%s\" tem um nome de arquivo inválido.";


/* End of file migration_lang.php */
/* Translate Pt-br by: Marcelo Leonízio - @mleonizio */ 
/* Location: ./system/language/english/migration_lang.php */