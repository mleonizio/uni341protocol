<?php

$lang['ftp_no_connection']			= "Não foi possível localizar um ID de conexão válido. Por favor, verifique se você está conectado antes de executar qualquer rotina de arquivo.";
$lang['ftp_unable_to_connect']		= "Não é possível conectar ao servidor de FTP usando o hostname fornecido.";
$lang['ftp_unable_to_login']		= "Impossível fazer login no seu servidor FTP. Por favor, verifique o seu nome de usuário e senha.";
$lang['ftp_unable_to_makdir']		= "Não é possível criar o diretório que você especificou.";
$lang['ftp_unable_to_changedir']	= "Não é possível alterar diretórios.";
$lang['ftp_unable_to_chmod']		= "Não foi possível definir permissões de arquivo. Por favor, verifique o seu caminho. Nota: Esta funcionalidade só está disponível em PHP 5 ou superior.";
$lang['ftp_unable_to_upload']		= "Incapaz de fazer o upload do arquivo especificado. Por favor, verifique o seu caminho.";
$lang['ftp_unable_to_download']		= "Não é possível baixar o arquivo especificado. Por favor, verifique o seu caminho.";
$lang['ftp_no_source_file']			= "Não foi possível localizar o arquivo de origem. Por favor, verifique o seu caminho.";
$lang['ftp_unable_to_rename']		= "Não é possível renomear o arquivo.";
$lang['ftp_unable_to_delete']		= "Não é possível excluir o arquivo.";
$lang['ftp_unable_to_move']			= "Não é possível mover o arquivo. Por favor, certifique-se existe o diretório de destino.";


/* End of file ftp_lang.php */
/* Translate Pt-br by: Marcelo Leonízio - @mleonizio */ 
/* Location: ./system/language/english/ftp_lang.php */